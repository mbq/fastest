

double pearson(double *x,double *y,int n){
 double sx,sy,sxy,sxx,syy;
 sx=sy=sxy=sxx=syy=0.;
 for(int e=0;e<n;e++){
  double xy=x[e]*y[e];
  sx+=x[e];
  sy+=y[e];
  sxy+=xy;
  sxx+=(x[e])*(x[e]);
  syy+=(y[e])*(y[e]);
 }
 return((n*sxy-sx*sy)/sqrt((n*sxx-sx*sx)*(n*syy-sy*sy)));
}
