typedef struct kw_arena {
	int n;
	int nY;
	double *xi;
	double *ranks;
	double *group_sums;
	int *perm;
} kw_arena_t;

kw_arena_t* alloc_kw_arena(int n,int nY,int nt){
	 kw_arena_t *ans=(kw_arena_t*)R_alloc(sizeof(kw_arena_t),nt);
	 for(int e=0;e<nt;e++){
		 ans[e].n=n;
		 ans[e].xi=(double*)R_alloc(sizeof(double),n);
		 ans[e].nY=nY;
		 ans[e].ranks=(double*)R_alloc(sizeof(double),n);
		 ans[e].group_sums=(double*)R_alloc(sizeof(double),nY);
		 ans[e].perm=(int*)R_alloc(sizeof(int),n);
	 }
	 return(ans);
}

double statistic_ties(double *ranks,int *y,int *yn,double *group_sums,int n,int nY){
 double dn=((double)n);
 double rnorm=(dn+1.)/2.,
	norm=0.;
 for(int e=0;e<nY;e++) group_sums[e]=0.;
 for(int e=0;e<n;e++){
  group_sums[y[e]-1]+=ranks[e];
  double p=(ranks[e]-rnorm);
  norm+=p*p;
 }
 double val=0.;
 for(int e=0;e<nY;e++){
  double p=group_sums[e]/((double)yn[e])-rnorm;
  val+=p*(p/norm)*((double)yn[e]);
 }
 return(val*(dn-1.));
}

void permute_kw(double *x,int *y,int *yn,kw_arena_t *arena,int m,rng_t *rng,double *value,int *c){
	int n=arena->n,
	    nY=arena->nY,
	    cc=0;
	double *xi=arena->xi,
	       *ranks=arena->ranks;
	for(int e=0;e<n;e++) if(!isnan(x[e])) xi[e]=x[e]; else {
		c[0]=0;
		value[0]=-17.;
	}
	rank(xi,ranks,arena->perm,n);
	//Re-purpose xi as permuttable Y
	int *yp=(int*)xi;
	for(int e=0;e<n;e++) yp[e]=y[e];
	double s0=statistic_ties(ranks,y,yn,arena->group_sums,n,nY);
        for(int e=0;e<m;e++){
		shuffle_int(yp,n,rng);
		cc+=s0<=statistic_ties(ranks,yp,yn,arena->group_sums,n,nY);
	}
	c[0]=cc;
	value[0]=s0;
}

double kw_kernel(double *x,int *y,int *yn,kw_arena_t *arena){
 int n=arena->n,
     nY=arena->nY;
 double *xi=arena->xi,
        *ranks=arena->ranks;
 //TODO: Add stride, xi[e]=x[e*stride], for row-wise & arbitrary dim

 for(int e=0;e<n;e++) if(!isnan(x[e])) xi[e]=x[e]; else return(-17.);

 rank(xi,ranks,arena->perm,n);
 //TODO: Add by-pass for no ties; rank shall detect them,
 // and emit a flag which can be used here.
 double val=statistic_ties(ranks,y,yn,arena->group_sums,n,nY);
 return(val);
}

SEXP C_kw_list(SEXP X,SEXP Y,SEXP Threads){
 if(!isList(X) && !isFrame(X)) error("X must be a list or a data.frame");
 int nt=parseThreads(Threads);
 int n=length(Y),
     m=length(X);
 double **xx=(double**)R_alloc(sizeof(double*),m);
 for(int e=0;e<m;e++){
  SEXP XX;
  PROTECT(XX=VECTOR_ELT(X,e));
  if(length(XX)!=n) error("Element of X has a bad length");
  //TODO: Coerce int into double here
  xx[e]=REAL(XX);
  UNPROTECT(1);
 }

 //Build Y table
 int nY=length(getAttrib(Y,R_LevelsSymbol));
 int *yn=(int*)R_alloc(sizeof(int),nY),
     *y=INTEGER(Y);
 for(int e=0;e<nY;e++) yn[e]=0;
 for(int e=0;e<n;e++) yn[y[e]-1]++;

 kw_arena_t *arenas=alloc_kw_arena(n,nY,nt);
 
 SEXP Ans;
 PROTECT(Ans=allocVector(REALSXP,m));
 double *ans=REAL(Ans);

 #pragma omp parallel num_threads(nt)
 {
  int tn=omp_get_thread_num();
  #pragma omp for
  for(int e=0;e<m;e++)
   ans[e]=kw_kernel(xx[e],y,yn,&(arenas[tn]));
 }

 UNPROTECT(1);
 return(Ans);
}

SEXP C_kw(SEXP X,SEXP Y){
 int n=length(Y);
 if(length(X)!=n) error("eee");

 int nY=length(getAttrib(Y,R_LevelsSymbol));
 int *yn=(int*)R_alloc(sizeof(int),nY),
     *y=INTEGER(Y);
 for(int e=0;e<nY;e++) yn[e]=0;
 for(int e=0;e<n;e++) yn[y[e]-1]++;

 double *x=REAL(X);
 double *xi=(double*)R_alloc(sizeof(double),n);
 for(int e=0;e<n;e++) xi[e]=x[e];

 kw_arena_t *arena=alloc_kw_arena(n,nY,1);
 
 SEXP Ans;
 PROTECT(Ans=allocVector(REALSXP,1));
 double *ans=REAL(Ans);
 ans[0]=kw_kernel(x,y,yn,arena);


 UNPROTECT(1);
 return(Ans);
}

SEXP C_kw_perm(SEXP X,SEXP Y,SEXP M){
 int n=length(Y);
 if(length(X)!=n) error("eee");
 if(length(M)!=1) error("Invalid m");
 int *_m=INTEGER(M);
 int m=_m[0];

 int nY=length(getAttrib(Y,R_LevelsSymbol));
 int *yn=(int*)R_alloc(sizeof(int),nY),
     *y=INTEGER(Y);
 for(int e=0;e<nY;e++) yn[e]=0;
 for(int e=0;e<n;e++) yn[y[e]-1]++;

 double *x=REAL(X);
 double *xi=(double*)R_alloc(sizeof(double),n);
 for(int e=0;e<n;e++) xi[e]=x[e];

 uint64_t seed=seed_from_R();
 rng_t rng;
 init(&rng,seed,0);
 
 kw_arena_t *arena=alloc_kw_arena(n,nY,1);

 SEXP Ans;
 PROTECT(Ans=allocVector(REALSXP,2));
 double *ans=REAL(Ans);

 double val=-177.;
 int count=-177;
 permute_kw(x,y,yn,arena,m,&rng,&val,&count);

 ans[0]=val;
 ans[1]=(double)count;


 UNPROTECT(1);
 return(Ans);
	
}

//TODO: Move to side.h
SEXP C_rank_order(SEXP X){
 int n=length(X);

 SEXP Ans,Ranks,Order;
 PROTECT(Ans=allocVector(VECSXP,2));
 PROTECT(Ranks=allocVector(REALSXP,n));
 PROTECT(Order=allocVector(INTSXP,n));

 SET_VECTOR_ELT(Ans,0,Ranks);
 SET_VECTOR_ELT(Ans,1,Order);
 double *ranks=REAL(Ranks);
 int *order=INTEGER(Order);

 double *xi=(double*)R_alloc(sizeof(double),n);

 if(isInteger(X)){
  int *x=INTEGER(X);
  for(int e=0;e<n;e++) if(x[e]!=NA_INTEGER) xi[e]=(double)x[e]; else error("NAs not allowed!");
 }else if(isNumeric(X)){
  double *x=REAL(X);
  for(int e=0;e<n;e++) if(!isnan(x[e])) xi[e]=x[e]; else error("No NAs nor NaNs allowed!");
 }else{
  error("X must be numeric or integer");
 }

 rank(xi,ranks,order,n);

 UNPROTECT(3);
 return(Ans);
}
