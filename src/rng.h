#include <stdint.h>

//PCG RNG by Melissa E. O'Neill https://www.pcg-random.org/pdf/hmc-cs-2014-0905.pdf
struct rng{
 uint64_t state;
 uint32_t stream;
};
typedef struct rng rng_t;

static inline uint32_t generate(rng_t *rng){
 rng->state=rng->state*6364136223846793005+rng->stream;
 uint32_t rot=(rng->state)>>59;
 uint32_t s=(((rng->state)>>18)^(rng->state))>>27;
 return((s<<((-rot)&31))|(s>>rot));
}

static inline void init(rng_t *rng,uint64_t seed,uint64_t stream){
 rng->state=seed;
 rng->stream=stream*2+1;
 generate(rng);
}

//Fetch seed from R's RNG
uint64_t seed_from_R(){
 GetRNGstate(); 
 uint64_t a=(uint32_t)(((double)(~((uint32_t)0)))*unif_rand()); 
 uint64_t b=(uint32_t)(((double)(~((uint32_t)0)))*unif_rand()); 
 PutRNGstate(); 
 return((a<<32)+b);
}


//Fast & unbiased algorithm by Daniel Lemire https://arxiv.org/pdf/1805.10941.pdf
static inline uint32_t randidx(rng_t *rng,uint32_t upto){
 uint32_t x=generate(rng);
 uint64_t m=((uint64_t)x)*((uint64_t)upto);
 uint32_t l=((uint32_t)m);
 if(l<upto){
  uint32_t t=(-upto)%upto;
  while(l<t){
   x=generate(rng);
   m=((uint64_t)x)*((uint64_t)upto);
   l=((uint32_t)m);
  }
 }
 return(m>>32);
}

//Fisher-Yates shuffle
static inline void shuffle(double *x,int n,rng_t *rng){
 for(int e=n-1;e>=1;e--){
  int ee=randidx(rng,e+1);
  double tmp=x[ee];
  x[ee]=x[e];
  x[e]=tmp;
 }
}

static inline void shuffle_int(int *x,int n,rng_t *rng){
 for(int e=n-1;e>=1;e--){
  int ee=randidx(rng,e+1),
      tmp=x[ee];
  x[ee]=x[e];
  x[e]=tmp;
 }
}

SEXP C_test_shuffle(SEXP X,SEXP SeedStream){
 if(!isInteger(SeedStream) || length(SeedStream)!=2)
  error("Invalid seed & stream");
 if(!isNumeric(X)) error("Invalid X");
 double *x=REAL(X);
 int n=length(X);
 if(n<2) return(X);

 SEXP Ans=PROTECT(allocVector(REALSXP,n));
 
 rng_t rng;
 int *ss=INTEGER(SeedStream);
 init(&rng,(uint32_t)(ss[0]),(uint32_t)(ss[1]));

 double *a=REAL(Ans);
 for(int e=0;e<n;e++) a[e]=x[e];
 shuffle(a,n,&rng);
 UNPROTECT(1);
 return(Ans);
}
