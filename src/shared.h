int parseThreads(SEXP Threads){
 int nt;
 if(isInteger(Threads) && length(Threads)!=1) error("Invalid threads argument");

 nt=INTEGER(Threads)[0];
 if(nt<0) error("Invalid threads argument");
 if(nt>omp_get_max_threads()){
  nt=omp_get_max_threads();
  warning("Thread count capped to %d",nt);
 }
 if(nt==0) nt=omp_get_max_threads();
 return(nt);
}

void rank(double *x,double *rnk,int *perm,int n){
 //Finally -> convert x-->rank
 for(int e=0;e<n;e++) perm[e]=e;
 //R-built-in sort with rank; max first, min last
 revsort(x,perm,n);
 double *xs=x,*xe=x;

 //TODO: Simplify this loop, use e
 for(int e=1;e<=n;e++){
  if(e==n || x[e]<*xe){
   //End of a block
   int is=xs-x;
   int ie=xe-x;
   double val=((double)is+(double)ie)/2;
   while(xs<=xe){
    *xs=val;
    xs++;
   }
   xe++;
  }else{
   //Block of same values continues
   xe++;
  }
 }
 for(int e=0;e<n;e++)
   rnk[perm[e]]=((double)n)-x[e];
}
