#include <R.h>
#include <Rdefines.h>
#include <Rinternals.h>
#include <R_ext/Utils.h>
#include <R_ext/Rdynload.h>
#include <R_ext/Visibility.h> 

#ifdef _OPENMP
 #include <omp.h>
#else
 #define omp_get_thread_num() 0
 #define omp_get_max_threads() 1
 #define omp_set_num_threads(x)
#endif

#include "shared.h"
#include "rng.h"

#include "pear.h"
#include "kw.h"

#define CALLDEF(name, n)  {#name, (DL_FUNC) &name, n}
static const R_CallMethodDef R_CallDef[]={
 CALLDEF(C_kw,2),
 CALLDEF(C_kw_perm,3),
 CALLDEF(C_kw_list,3),
 CALLDEF(C_rank_order,1),
 CALLDEF(C_test_shuffle,2),
 {NULL,NULL,0}
};

void attribute_visible R_init_fastest(DllInfo *dll){
 R_registerRoutines(dll,NULL,R_CallDef,NULL,NULL);
 R_useDynamicSymbols(dll,FALSE);
 R_forceSymbols(dll,TRUE);
}
